import datetime
import json

from django.db.models import Q, Avg, Sum, F, Count, Case, IntegerField, When
from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import render
from rest_framework.generics import ListAPIView, RetrieveDestroyAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import CreateCouriersSerializer, CourierSerializer, OrderSerializer, CreateOrdersSerializer, \
    OrderCompleteSerializer, CourierExtSerializer, Assign4Courier
from .models import Courier, Order
from . import services
from . import paginators


class CourierListView(ListAPIView):
    """
    Класс для вывода списка курьеров и добавления нового
    """

    # выводить будем только топ курьеров по рейтингу по каждому типу
    serializer_class = CourierSerializer
    queryset = (
                   Courier.objects.filter(capacity=10).annotate(
                    mean_score=Avg("orders__score")
                    ).order_by(F("mean_score").desc(nulls_last=True))[:5]
                ) | (Courier.objects.filter(capacity=15).annotate(
                    mean_score=Avg("orders__score")
                    ).order_by(F("mean_score").desc(nulls_last=True))[:5]
                ) | (Courier.objects.filter(capacity=50).annotate(
                    mean_score=Avg("orders__score")
                    ).order_by(F("mean_score").desc(nulls_last=True))[:5])

    def post(self, request):
        """
        Создание нового курьера
        :param request: информацию о новом курьере
        :return: данные нового курьера или ошибки создания
        """
        couriers = CreateCouriersSerializer(data=self.request.data)
        if couriers.is_valid():
            couriers = couriers.save()
            return JsonResponse(status=201, data=couriers, safe=False)
        return JsonResponse(status=400, data=couriers.errors, safe=False)


class CourierView(RetrieveUpdateDestroyAPIView):
    """
    Удаление, изменение курьера по ид
    """
    serializer_class = CourierSerializer
    queryset = Courier.objects.all()

    def partial_update(self, request, *args, **kwargs):
        """
        Обновление курьера
        :param request: новая информация о курьере
        :param kwargs: содержит Ид изменяемого курьера
        :return: данные нового курьера или ошибки создания
        """
        current_courier = self.get_object()

        # словарь с изменениями, нужно проверить что все текущие заказы подходят
        changes = services.make_patch_changes(request.data, current_courier)

        # открепить заказы
        services.detach_orders(current_courier.id, changes)

        return super().partial_update(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        """
        Возвращает расширенную информацию по курьеру
        :param request: запрос
        :param kwargs: содержит Ид курьера
        :return: информация о курьере
        """
        courier = self.get_ext_courier(**kwargs)
        res = CourierExtSerializer(courier).data
        return JsonResponse(status=200, data=res)

    def get_ext_courier(self, **kwargs):
        """
        Получение курьера с дополнениями
        :param kwargs: Ид курьера
        :return: courier с
            средним рейтингом,
            количеством выполненных заказов,
            среднем времени доставки,
            количеством заработанных денег
        """
        return (Courier.objects
                .annotate(rating=Avg("orders__score"))
                .annotate(earning=Sum("orders__price"))
                .annotate(mean_time=Avg(
                    F("orders__deliver_time") - F("orders__take_time"),
                    filter=Q(orders__deliver_time__isnull=False)
                    )
                )
                .annotate(cnt_completed_orders=Count(
                    "orders",
                    filter=Q(orders__deliver_time__isnull=False))
                )
                .get(**kwargs))


class OrderListView(ListAPIView):
    """
    Вывод списка заказов и добавления нового
    """
    queryset = Order.objects.all().select_related("courier").order_by("id")
    serializer_class = OrderSerializer
    pagination_class = paginators.OrderPaginator

    def post(self, request):
        """
        Создание нового заказа
        :param request: информацию о новом курьере
        :return: данные нового заказа или ошибки создания
        """
        orders = CreateOrdersSerializer(data=self.request.data)
        if orders.is_valid():
            orders = orders.save()
            return JsonResponse(status=201, data=json.dumps(orders), safe=False)
        return JsonResponse(status=400, data=json.dumps(orders.errors), safe=False)


class OrderAssign(APIView):
    def post(self, request):
        """
        Для заданного курьера проставляет ему максимально возможное количество заказов
        :param request: содержит информацию об ид курьера
        :return возвращает список ид заказов и время начала выполнения
        """
        data = Assign4Courier(self.request.data).data
        pk = data["courier_id"]
        courier = get_object_or_404(Courier.objects.all(), id=pk)

        # находим заказы подходящие по весу, району сбора и доставки
        orders = Order.objects.filter(
            (
                Q(courier_id__isnull=True) &
                Q(weight__lte=courier.capacity) &
                (Q(deliver_from__in=courier.district) |
                 Q(deliver_to__in=courier.district))
            )
            # также добавим заказы которые курьер уже взял, но не выполнил
            | (
                Q(courier_id=pk)
                & Q(deliver_time__isnull=True)
            )
        ).all()
        old_orders, new_orders = services.filter_with_work_time(orders, courier.work_time)

        res_orders = []
        take_time = None
        if new_orders:
            take_time = datetime.datetime.now()
            Order.objects.filter(id__in=new_orders).update(courier=courier, take_time=take_time)
            res_orders = [{"id": order_id} for order_id in new_orders]

        # при отсутствии новых заказов возвращается время принятия произвольного заказа
        elif old_orders:
            res_orders = [{"id": order_id} for order_id in old_orders]
            take_time = orders.first().take_time
        res = {"orders": res_orders}
        if res_orders:
            res["assign_time"] = str(take_time)
        return JsonResponse(res, safe=False)


class CompleteOrder(APIView):
    """
    Завершение заказа
    """
    def post(self, request):
        """
        Выполнение заказа курьером
        :param request:
                data, courier_id, order_id, complete_time, score
        :return: словарь с Ид выполненного заказа
        """
        data = OrderCompleteSerializer(request.data).data
        exist_courier = Courier.objects.filter(id=data["courier_id"]).exists()
        if not exist_courier:
            return HttpResponseBadRequest("Not courier")

        correct_orders = Order.objects.filter(courier_id=data["courier_id"], id=data["order_id"])[:1]
        if not correct_orders:
            return HttpResponseBadRequest("Not order")

        correct_order = correct_orders[0]
        if not correct_order.deliver_time and correct_order.score:
            correct_order.deliver_time = data["complete_time"]
            correct_order.score = data["score"]
            correct_order.save()
        return JsonResponse({"id": data["order_id"]}, status=200)
