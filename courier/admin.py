from django.contrib import admin
from .models import Courier, Order
# Register your models here.


admin.site.register(Courier)
admin.site.register(Order)

