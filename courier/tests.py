import json

import pytest

import pytest
from django.urls import reverse
from .models import Courier, Order


@pytest.fixture
def create_db():
    Courier(**{'id': 1, 'capacity': 10,
               'district': [1, 2], 'work_time': ['12:00-18:00']}).save()
    Courier(**{'id': 2, 'capacity': 15,
               'district': [2, 3], 'work_time': ['12:00-15:00']}).save()
    Courier(**{'id': 3, 'capacity': 50,
               'district': [1, 2], 'work_time': ['15:00-18:00']}).save()
    Order(**{
        "weight": 10,
        "deliver_from": 1,
        "deliver_to": 3,
        "price": 100,
        "take_time": None,
        "receiver_time": "2013-12-25T17:01:00",
    }).save()

    Order(**{
        "weight": 30,
        "deliver_from": 1,
        "deliver_to": 3,
        "price": 100,
        "take_time": None,
        "receiver_time": "2013-12-25T17:00:00",
    }).save()

    Order(**{
        "weight": 10,
        "deliver_from": 2,
        "deliver_to": 2,
        "price": 100,
        "take_time": None,
        "receiver_time": "2013-12-25T14:00:00",
    }).save()

    Order(**{
        "weight": 40,
        "deliver_from": 2,
        "deliver_to": 3,
        "price": 100,
        "take_time": None,
        "receiver_time": "2013-12-25T17:00:00",
    }).save()


@pytest.mark.django_db
@pytest.mark.parametrize(
    'cnt, ids, courier_id', [
        (2, [1, 3], 1),
        (1, [3], 2),
        (3, [1, 2, 4], 3),
    ]
)
def test_assign_order(client, cnt, ids, courier_id, create_db):
    """
    Проверка назначения заказов курьеру
    :param client:
    :param cnt:
    :param ids:
    :param courier_id:
    :param create_db:
    :return:
    """
    response = client.post('http://127.0.0.1:8000/api/orders/assign/',
                           data={"courier_id": courier_id})

    res = json.loads(response.content)["orders"]
    assert len(res) == cnt

    assigned = [i["id"] for i in res]
    assert assigned == ids


@pytest.mark.django_db
@pytest.mark.parametrize(
    'field_name, val, order_ids', [
        ("district", [1], [1, 2]),
        ("capacity", 10, [1]),
        ("work_time", ['17:01-18:00'], [1])
    ]
)
def test_patch_courier(client, field_name, val, order_ids, create_db):
    """
    Тестировать только при правильной работе test_assign_order
    :param client:
    :param field_name:
    :param val:
    :param order_ids:
    :param create_db:
    :return:
    """
    response = client.post('http://127.0.0.1:8000/api/orders/assign/',
                           data={"courier_id": 3})

    response = client.patch('http://127.0.0.1:8000/api/couriers/3/',
                            data={field_name: val}, content_type='application/json')

    assert response.status_code == 200
    res = [i for i in Courier.objects.get(id=3).orders.values_list(flat=True)]
    assert sorted(res) == order_ids


@pytest.mark.django_db
def test_view_put_courier(client):
    Courier(**{'id': 1, 'capacity': 10,
               'district': [1, 2, 3], 'work_time': ['12:00-15:00']}).save()
    response = client.put('http://127.0.0.1:8000/api/couriers/1/',
                          data={'capacity': 50,
                                'district': [1, 2, 3],
                                'work_time': ['12:00-15:00']},
                          content_type='application/json')
    assert response.status_code == 200


@pytest.mark.django_db
def test_view_post_courier(client):
    # проверка корректного и некорекктного создания курьера
    data = [
        {"data": [
            {
                "capacity": 50,
                "district":
                    [1, 2, 3],
                "work_time":
                    ["12:00-15:00"]
            }
        ]},
        {"data": [
            {
                "capacity": 500,
                "district":
                    [1, 2, 3]
                ,
                "work_time":
                    ["12:00-15:00"]
            }
        ]},
        {"data": [
            {
                "capacity": 500,
                "district":
                    [1, 2, 3]
                ,
                "work_time":
                    ["12:00-15:00", "15:00-19:00"]
            }
        ]}
    ]
    statuses = [201, 400, 400]

    for status, current_data in zip(statuses, data):
        response = client.post('http://127.0.0.1:8000/api/couriers/',
                               data=current_data,
                               content_type='application/json'
                               )
        assert response.status_code == status


@pytest.mark.django_db
def test_view_post_order(client):
    data = [
        {"data": [
            {
                "weight": 50,
                "deliver_to": 1,
                "deliver_from": 1,
                "price": "300.0",
                "receiver_time": "2013-12-25T17:00:00"

            }
        ]},
        {"data": [
            {
                "weight": 50,
                "deliver_to": 1,
                "deliver_from": 1,
                "price": "300.0",
                "receiver_time": "2013-12-25T17:00:00"

            }
        ]},
        {"data": [
            {
                "weight": 50,
                "deliver_to": 1,
                "deliver_from": 1,
                "price": "300.0",
                "receiver_time": "2013-12-25T17:00:00"

            },
            {
                "weight": 150,
                "deliver_to": "1212",
                "deliver_from": 1,
                "price": "300.0",
                "receiver_time": "2013-12-25T17:00:00"

            }
        ]},
        {"data": [
            {
                "weight": "1221ds",
                "deliver_to": 1,
                "deliver_from": 1,
                "price": "300.0",
                "receiver_time": "2013-12-25T17:00:00"

            },
            {
                "weight": 150,
                "deliver_to": "1212",
                "deliver_from": 1,
                "price": "300.0",
                "receiver_time": "2013-12-25T17:00:00"

            }
        ]}
    ]
    statuses = [201, 201, 201, 400]

    for status, current_data in zip(statuses, data):
        response = client.post('http://127.0.0.1:8000/api/orders/',
                               data=current_data,
                               content_type='application/json')
        print(current_data)
        assert response.status_code == status


@pytest.mark.django_db
def test_complete_order(client, create_db):
    """
    Тестировать только при правильной работе test_assign_order
    :param client:
    :param field_name:
    :param val:
    :param order_ids:
    :param create_db:
    :return:
    """
    inp = [
        {
            "courier_id": 2,
            "order_id": 3,
            "complete_time": "2021-01-10T10:33:01.42Z",
            "score": 5
        },
        {
            "courier_id": 111,
            "order_id": 2,
            "complete_time": "2021-01-10T10:33:01.42Z",
            "score": 5
        },
        {
            "courier_id": 111,
            "order_id": 2,
            "complete_time": "2021-01-10T10:33:01.42Z",
            "score": 5
        }]
    response = client.post('http://127.0.0.1:8000/api/orders/assign/',
                           data={"courier_id": 2})
    statuses = [200, 400, 400]
    for data, status in zip(inp, statuses):
        response = client.post('http://127.0.0.1:8000/api/orders/complete/',
                               data=data)
        q = response.__dict__
        orders = [ordd.courier_id for ordd in Order.objects.all()]
        assert response.status_code == status


