from django.urls import path
from . import views

urlpatterns = [
    path('couriers/', views.CourierListView.as_view(), name='courier'),
    path('couriers/<int:pk>/', views.CourierView.as_view(), name='single_courier'),
    path('orders/', views.OrderListView.as_view(), name='orders'),
    path('orders/assign/', views.OrderAssign.as_view(), name='assign_orders'),
    path('orders/complete/', views.CompleteOrder.as_view(), name='complete_order')
]