from rest_framework import serializers

from .models import Courier, Order
from .services import to_time
from functools import reduce
import datetime


class CreateSingleCourierSerializer(serializers.ModelSerializer):
    """
    Сериализатор создания одного курьера
    """
    class Meta:
        model = Courier
        fields = '__all__'

    def validate_work_time(self, value):
        """
        Время работы должно приходить в формате списка,
        каждый элемент - строка период работы в формате hh:mm-hh:mm начало-конец
        Суммарное количество <= 8 часов
        """
        if not isinstance(value, list):
            raise serializers.ValidationError("Field can contain only work_time")
        if (reduce(lambda val, acc: acc+val,
                   [end - start for start, end in [to_time(val) for val in value]],
                   datetime.timedelta()
                   ) > datetime.timedelta(hours=8)):
            raise serializers.ValidationError("Working hours exceed the permissible duration")

        return value

    def validate_district(self, value):
        """
        Значение должно приходить в формате списка, каждый элемент - целое число
        """
        if not isinstance(value, list) or any(not isinstance(district, int) for district in value):
            raise serializers.ValidationError("Field can contain only district")
        return value


class CreateCouriersSerializer(serializers.Serializer):
    """
    Создание списка курьеров
    """
    data = CreateSingleCourierSerializer(many=True)

    def create(self, validated_data):
        couriers = Courier.objects.bulk_create(
            [Courier(**courier_data) for courier_data in validated_data.get('data', [])])

        # если все данные верны возвращаем список созданных id
        return {"couriers": [{"id": courier.id} for courier in couriers]}


class CourierSerializer(serializers.ModelSerializer):
    """
    Сериализатор курьера
    """
    class Meta:
        model = Courier
        fields = '__all__'


class CreateSingleOrderSerializer(serializers.ModelSerializer):
    """
    Сериализатор заказов
    """
    class Meta:
        model = Order
        exclude = ("deliver_time", "courier")


class CreateOrdersSerializer(serializers.Serializer):
    data = CreateSingleOrderSerializer(many=True)

    def create(self, validated_data):
        orders = Order.objects.bulk_create(
            [Order(**order_data) for order_data in validated_data.get('data', [])])

        # если все данные верны возвращаем список созданных id
        return {"orders": [{"id": orders.id} for orders in orders]}


class OrderSerializer(serializers.ModelSerializer):
    courier = CourierSerializer()

    class Meta:
        model = Order
        fields = "__all__"


class OrderCompleteSerializer(serializers.Serializer):
    """
    Сериализатор выполнения заказа
    """
    courier_id = serializers.IntegerField()
    order_id = serializers.IntegerField()
    complete_time = serializers.DateTimeField()
    score = serializers.IntegerField()


class CourierExtSerializer(serializers.ModelSerializer):
    """
    Сериализатор вывода расширенной информации по курьеру
    """
    rating = serializers.DecimalField(max_digits=4, decimal_places=2)
    earning = serializers.DecimalField(max_digits=8, decimal_places=2)
    mean_time = serializers.DurationField()
    cnt_completed_orders = serializers.IntegerField()

    class Meta:
        model = Courier
        fields = '__all__'


class Assign4Courier(serializers.Serializer):
    """
    Сериализатор входных данных для назначения заказов курьеру
    """
    courier_id = serializers.IntegerField()
