import datetime
import json

from django.db import models

# Create your models here.


class Courier(models.Model):
    """Модель курьера"""
    class Capacity(models.IntegerChoices):
        LOW = 10, 'foot'
        NORMAL = 15, 'bike'
        HIGH = 50, 'car'
    work_time = models.JSONField('Время работы')
    district = models.JSONField('Районы доставки')
    capacity = models.IntegerField('Грузоподьемность', choices=Capacity.choices)


class Order(models.Model):
    """Модель заказа"""
    weight = models.SmallIntegerField('Вес')
    deliver_from = models.SmallIntegerField("Место получения заказа")
    deliver_to = models.SmallIntegerField("Место доставки заказа")
    price = models.DecimalField('Стоимость', max_digits=7, decimal_places=2)
    take_time = models.DateTimeField('Время принятия заказа курьером', null=True, blank=True)
    receiver_time = models.DateTimeField('Время назначения заказа')
    deliver_time = models.DateTimeField('Время отдачи заказа', null=True, blank=True)
    score = models.SmallIntegerField('Оценка', null=True, blank=True)
    courier = models.ForeignKey(Courier, on_delete=models.SET_NULL, related_name='orders', null=True, blank=True)

