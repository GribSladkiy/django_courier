from . import models
from django.db.models.query import QuerySet
import datetime
from django.db.models import Q


def to_time(work_time: str):
    """
    Возвращает время работы курьера в виде python объектов
    :param work_time: время работы курьера в виде строки
    """
    return [datetime.datetime.strptime(current_time, "%H:%M") for current_time in work_time.split('-')]


def right_time(order, work_times):
    """
    Подходит по времени работы или нет
    :param order: заказ
    :param work_times: время работы курьера
    :return: bool
    """
    for work_time in work_times:
        start, end = to_time(work_time)
        if start.time() <= order.receiver_time.time() <= end.time():
            return True
    return False


def filter_with_work_time(orders: QuerySet, work_times: list):
    """
    Поиск заказов подходящих по времени работы
    :param orders: заказы
    :param work_times: время работы курьера
    :return:
        уже взятые заказы,
        новые заказы
    """
    already_taken_ids = []
    new_orders_ids = []
    for order in orders:
        if order.courier_id:
            already_taken_ids.append(order.id)
            continue
        if right_time(order, work_times):
            new_orders_ids.append(order.id)
    return already_taken_ids, new_orders_ids


def make_patch_changes(new_data: dict, obj):
    """
    :param new_data: словарь с новыми значениями для объекта
    :param obj: объект модели
    :return: словарь с изменениями в формате old, new value
    """
    return {
            key: {
                "old": getattr(obj, key),
                "new": value
            } for key, value in new_data.items()
        }


def filter_with_diff_time(orders: QuerySet, changes: dict):
    """
    Поиск заказов курьера неподходящих под новое состояние
    :param orders: набор заказов
    :param changes: изменение курьера
    :return: список Ид неподходящих заказов
    """

    # время точно изменилось
    new_time = changes.get("work_time")["new"]
    new_district = changes.get("district", {}).get("new")
    new_capacity = changes.get("capacity", {}).get("new")
    ids = []
    for order in orders:
        if (
                (new_capacity and order.weight > new_capacity) or
                (new_district and (order.deliver_to not in new_district and order.deliver_from not in new_district)) or
                not right_time(order, new_time)
        ):
            ids.append(order.id)
    return ids


def detach_orders(courier_id: int, changes: dict):
    """
    Открепление заказов у курьера
    :param changes: словарь с изменениями
    :param courier_id: Ид изменяемого курьера
    """
    courier = Q(courier_id=courier_id) | Q(deliver_time__isnull=True)

    if changes.get("work_time"):

        # если изменилось время, то сразу неподходящие заказы не определить
        # придется доставать все записи и фильтровать
        orders = models.Order.objects.filter(courier)
        order_ids = filter_with_diff_time(orders, changes)
        detached_orders = models.Order.objects.filter(id__in=order_ids)
    else:

        # если не поменялось время отфильтруем сразу неподходящие заказы
        capacity_query = Q()
        district_query = Q()
        if changes.get("capacity"):
            capacity_query = Q(weight__gt=changes.get("capacity")["new"])

        if changes.get("district"):
            district = changes.get("district")["new"]
            district_query = (~Q(deliver_from__in=district) &
                              ~Q(deliver_to__in=district))
        detached_orders = models.Order.objects.filter(courier & (capacity_query | district_query))

    detached_orders.update(courier_id=None, take_time=None)
